import QtQuick 2.0

Rectangle {
    width: 30
    height: width
    radius: width / 2
    color: "blue"
    antialiasing: false

    signal out(real x, real y)

    property var angle: 0
    property var step: 2
    property var index: 1

    function init() {
        x = parent.width / 2 - width / 2
        y = parent.height / 2 - height / 2
        angle = pickIn(0, 2 * Math.PI)

        if(angle < 0.5 ||
                (angle <= Math.PI / 2 + 0.5 && angle >= Math.PI / 2 - 0.5) ||
                (angle <= Math.PI + 0.5 && angle >= Math.PI + 0.5) ||
                (angle <= 3 * Math.PI / 2 + 0.5 && angle >= 3 * Math.PI / 2 + 0.5)) {
            angle += Math.PI / 4
        }
    }

    function move() {
        y = y + (step * Math.cos(angle))
        x = x + (step * Math.sin(angle))

        if(x < 0) {
            out(x, y)
        } else if (x > parent.width - width) {
            out(x, y)
        }

        if (y < 0 || y > parent.height - height) {
            h_reflect()
        }
    }

    function h_reflect() {
        if (angle > Math.PI) {
            angle = 3 * Math.PI - angle
        } else {
            angle = Math.PI - angle
        }
        x -= 1
    }

    function v_reflect() {
        angle = 2 * Math.PI - angle
        var max = angle * index
        var min = angle - (max - angle)
        index = 1
        angle = pickIn(min, max)
    }

    function pickIn(min, max) {
        return Math.random() * (max - min) + min
    }
}
