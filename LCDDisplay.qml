import QtQuick 2.0
import QtQuick.Layouts 1.12

Rectangle {
    id: display
    width: 174
    height: 146
    border.width: 3
    border.color: "black"
    color: "green"

    Item {
        id: internal
        property real value: 0
    }

    RowLayout {
        id: holder
        spacing: 4
        width: 80
        height: 136


        LCDDigit {
            id: tens
            Layout.bottomMargin: 5
            Layout.leftMargin: 5
            Layout.topMargin: 5
            textScale: 4
            color: display.color
        }

        LCDDigit {
            id: units
            Layout.bottomMargin: 5
            Layout.topMargin: 5
            Layout.rightMargin: 5
            textScale: 4
            color: display.color
        }
    }

    function inc() {
        if (units.value() === 9 && tens.value() === 9) {
            return
        }

        units.inc()

        if (units.value() === 0) {
            tens.inc()
        }

        internal.value += 1
    }

    function reset() {
        units.reset()
        tens.reset()

        internal.value = 0
    }

    function value() {
        return internal.value
    }
}
