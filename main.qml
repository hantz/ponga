import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12

Window {
    id: map
    visible: true
    width: 640
    height: 480
    color: "grey"
    title: qsTr("Ponga")

    property Paddle ai_paddle: right_paddle
    property Paddle player_paddle: left_paddle
    property bool playing: false

    Component.onCompleted: {
        ai_paddle.color = "red"
        player_paddle.color = "yellow"
        startScreen.open()
    }

    Timer {
        id: game_clock
        interval: 1
        repeat: true
        onTriggered: {
            ball.move()

            if ((ball.angle > Math.PI && touchingBall(left_paddle))
                    || touchingBall(right_paddle)) {
                ball.v_reflect()
            }
        }
    }

    Timer {
        id: ai_clock
        interval: 10
        repeat: true
        onTriggered: {
            ai_paddle.adjust(ball.y)
        }
    }

    Item {
        id: keyboardHandler
        focus: true
        Keys.onPressed: {
            if ((event.key === Qt.Key_Down || event.key === Qt.Key_Up)
                    && !playing)
                return

            switch (event.key) {
            case Qt.Key_Down:
                player_paddle.move(20)
                break
            case Qt.Key_Up:
                player_paddle.move(-20)
                break
            case Qt.Key_Space:
                if (playing) {
                    pauseGame()
                    pauseScreen.open()
                } else {
                    pauseScreen.close()
                    startLoops()
                }
            }
        }
    }

    LCDDisplay {
        id: player_score
        x: map.width / 4 - width / 2
        y: 10
    }

    LCDDisplay {
        id: ai_score
        x: 3 * map.width / 4 - width / 2
        y: 10
        color: "red"
    }

    Paddle {
        id: right_paddle
        visible: false
        anchors.right: parent.right
        y: map.height / 2 - height / 2
    }

    Paddle {
        id: left_paddle
        visible: false
        anchors.left: parent.left
        y: map.height / 2 - height / 2
    }

    Column {
        x: -1
        spacing: 4
        Repeater {
            property var itemHeight: 50
            model: Math.ceil(map.height / (itemHeight + parent.spacing))

            Rectangle {
                width: 10
                height: 50
                x: map.width / 2 - width / 2
                opacity: 0.3
            }
        }
    }

    Ball {
        id: ball
        visible: false
        onOut: {
            if (angle > Math.PI) {
                ai_score.inc()
            } else {
                player_score.inc()
            }

            if (player_score.value() === 1 || ai_score.value() === 1) {
                endGame()
            } else {
                initGame()
            }
        }
    }

    Popup {
        id: startScreen

        parent: Overlay.overlay
        x: Math.round((parent.width - width) / 2)
        y: Math.round((parent.height - height) / 2)
        width: parent.width / 4
        height: parent.height / 4
        background: Rectangle {
            color: "transparent"
        }

        Row {
            spacing: 20
            Button {
                text: "Play"
                onClicked: {
                    startScreen.close()
                    keyboardHandler.focus = true
                    map.startGame()
                }
            }

            Button {
                text: "Quit"
                onClicked: {
                    Qt.quit()
                }
            }
        }
    }

    Popup {
        id: pauseScreen

        parent: Overlay.overlay
        x: Math.round((parent.width - width) / 2)
        y: Math.round((parent.height - height) / 2)
        width: parent.width / 2
        height: parent.height / 2
        background: Rectangle {
            color: "transparent"
        }

        Row {
            spacing: 10
            anchors.centerIn: parent
            Repeater {
                model: 2
                anchors.fill: parent
                Rectangle {
                    color: "red"
                    width: pauseScreen.width / 4
                    height: pauseScreen.height / 2
                }
            }
        }
    }

    Popup {
        id: endScreen

        property string message: ""


        parent: Overlay.overlay
        x: Math.round((parent.width - width) / 2)
        y: Math.round((parent.height - height) / 2)
        width: parent.width / 2
        height: parent.height / 2
        Text {
            anchors.fill: parent
            anchors.centerIn: parent
            text: endScreen.message
        }

        Row {
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 10
            Button {
                text: "Play again"
                onClicked: {
                    endScreen.message = ""
                    endScreen.close()
                    keyboardHandler.focus = true
                    map.startGame()
                }
            }

            Button {
                text: "Quit"
                onClicked: {
                    Qt.quit()
                }
            }
        }

    }

    function pauseGame() {
        playing = false
        game_clock.stop()
        ai_clock.stop()
    }

    function endGame() {
        pauseGame()
        ball.visible = false
        var message = "You "
        message += ai_score.value() > player_score.value() ? "lost. " : "won. "
        message += player_score.value() + " : " + ai_score.value()
        endScreen.message = message
        endScreen.open()
    }

    function startLoops() {
        ai_clock.start()
        game_clock.start()
        playing = true
    }

    function initGame() {
        player_score.reset()
        ai_score.reset()
        ball.init()
        left_paddle.init()
        right_paddle.init()

        startLoops()
    }

    function startGame() {
        ball.visible = true
        left_paddle.visible = true
        right_paddle.visible = true

        initGame()
    }

    function touchingBall(rect) {
        if (rect.x + rect.width < ball.x || rect.x > ball.x + ball.width
                || rect.y + rect.height < ball.y
                || rect.y > ball.y + ball.height) {
            return false
        }

        if (rect.y <= ball.y && rect.y + rect.height >= ball.y) {
            var ry = ball.y - rect.y
            var dry = Math.abs((rect.height / 2) - ry)
            var index = dry / rect.height

            ball.index = 1 + index
        }

        return true
    }
}
