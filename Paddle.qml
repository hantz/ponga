import QtQuick 2.0

Rectangle {
    width: 20
    height: 200
    antialiasing: false

    function move(dy) {
        if (dy < 0) {
            y = Math.max(y + dy, 0)
        } else {
            y = Math.min(y + dy, parent.height - height)
        }
    }

    function adjust(target) {
        if (target > y + height / 2) {
            move(5)
        } else if (target < y + height / 2) {
            move(-5)
        }
    }

    function init() {
        y = parent.height / 2 - height / 2
    }
}
