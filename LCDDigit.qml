import QtQuick 2.12

Rectangle {
    id: container
    color: "transparent"

    property real textScale: 1
    width: 20 * textScale
    height: 34 * textScale

    property color textColor: "#1f1f1f"

    Item {
        id: internal
        property real sw: container.width / 10
        property real hw: container.width - 2 * sw
        property real vsh: (container.height - 3 * sw) / 2
        property real value: 0
    }

    Rectangle {
        id: v1
        x: 0
        y: internal.sw
        width: internal.sw
        height: internal.vsh
        color: textColor
        anchors.left: parent.left
    }

    Rectangle {
        id: v2
        x: parent.width - internal.sw
        y: internal.sw
        width: internal.sw
        height: internal.vsh
        color: parent.textColor
        anchors.right: parent.right
    }

    Rectangle {
        id: v3
        x: 0
        y: 2 * internal.sw + internal.vsh
        width: internal.sw
        height: internal.vsh
        color: parent.textColor
        anchors.left: parent.left
    }

    Rectangle {
        id: v4
        x: parent.width - internal.sw
        y: 2 * internal.sw + internal.vsh
        width: internal.sw
        height: internal.vsh
        color: parent.textColor
        anchors.right: parent.right
    }

    Rectangle {
        id: h1
        x: internal.sw
        y: 0
        width: internal.hw
        height: internal.sw
        color: parent.textColor
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Rectangle {
        id: h2
        x: internal.sw
        y: internal.sw + internal.vsh
        width: internal.hw
        height: internal.sw
        color: "transparent"
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Rectangle {
        id: h3
        x: internal.sw
        y: parent.height - internal.sw
        width: internal.hw
        height: internal.sw
        color: parent.textColor
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }

    function value() {
        return internal.value
    }

    function inc() {
        switch(internal.value) {
        case 0:
            v1.color = "transparent"
            v3.color = "transparent"

            h1.color = "transparent"
            h3.color = "transparent"
            break
        case 1:
            v4.color = "transparent"

            v3.color = textColor

            h1.color = textColor
            h2.color = textColor
            h3.color = textColor
            break
        case 2:
            v3.color = "transparent"

            v4.color = textColor
            break
        case 3:
            v1.color = textColor

            h1.color = "transparent"
            h3.color = "transparent"
            break
        case 4:
            v2.color = "transparent"

            h1.color = textColor
            h3.color = textColor
            break
        case 5:
            v3.color = textColor
            break
        case 6:
            v1.color = "transparent"
            v3.color = "transparent"

            v2.color = textColor

            h2.color = "transparent"
            h3.color = "transparent"
            break
        case 7:
            v1.color = textColor
            v3.color = textColor

            h2.color = textColor
            h3.color = textColor
            break
        case 8:
            v3.color = "transparent"
            break
        case 9:
            v3.color = textColor

            h2.color = "transparent"
            internal.value = 0
            return
        default:
            break
        }

        internal.value += 1
    }

    function reset() {
        v1.color = textColor
        v2.color = textColor
        v3.color = textColor
        v4.color = textColor

        h1.color = textColor
        h3.color = textColor

        h2.color = "transparent"

        internal.value = 0
    }
}
